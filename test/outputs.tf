# aurora_cluster/outputs

output "rds" {
  value     = module.aurora_cluster
  sensitive = true
}

output "secrets" {
  value     = module.secrets
  sensitive = true
}

