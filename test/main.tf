#  ws aurora_cluster/main.tf

module "aurora_cluster" {
  source                                         = "git::https://gitlab.com/mosar-infra/tf-module-aurora-cluster.git?ref=tags/v1.0.4"
  availability_zones                             = local.availability_zones
  backtrack_window                               = 0 # off
  backup_retention_period                        = 7
  cluster_identifier                             = "mosar-db-${var.environment}"
  copy_tags_to_snapshot                          = true
  database_name                                  = "flashcards"
  db_subnet_group_name                           = aws_db_subnet_group.mosar_rds_subnet_group.name
  deletion_protection                            = false
  enabled_cloudwatch_logs_exports                = ["audit", "error", "general", "slowquery"]
  enable_http_endpoint                           = true
  engine                                         = "aurora-mysql"
  engine_mode                                    = "serverless"
  engine_version                                 = "5.7.12"
  final_snapshot_identifier                      = "mosar-db-backup-${var.environment}"
  iam_database_authentication_enabled            = true
  master_password                                = module.secrets.secrets_versions["db-master-password"].secret_string
  master_username                                = "root"
  preferred_maintenance_window                   = "Mon:00:00-Mon:01:00"
  scaling_configuration_auto_pause               = true
  scaling_configuration_max_capacity             = 16
  scaling_configuration_min_capacity             = 1
  scaling_configuration_seconds_until_auto_pause = 300
  scaling_configuration_timeout_action           = "RollbackCapacityChange"
  skip_final_snapshot                            = true
  storage_encrypted                              = true
  vpc_security_group_ids                         = module.security_groups.security_groups["aurora_cluster"].*.id
  tags = {
    ManagedBy   = var.managed_by
    Environment = var.environment
  }
}

module "security_groups" {
  source          = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment     = var.environment
  managed_by      = var.managed_by
  security_groups = local.security_groups
}

resource "aws_db_subnet_group" "mosar_rds_subnet_group" {
  name       = "mosar_aurora_subnet_group"
  subnet_ids = [for s in data.aws_subnet.private : s.id]
  tags = {
    Name        = "mosar_aurora_sng"
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}

module "secrets" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-secrets.git?ref=tags/v3.1.2"
  secrets     = local.secrets
  environment = var.environment
  managed_by  = var.managed_by
}
