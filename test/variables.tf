#  ws rds/variables.tf

variable "environment" {
  default = "test"
}
variable "managed_by" {
  default = "aurora_cluster"
}
